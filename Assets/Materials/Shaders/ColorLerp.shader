﻿Shader "Pirates/ColorLerp" {
	Properties {
		_Color1("Color 1", Color) = (1, 1, 1, 1)
		_Color2("Color 1", Color) = (1, 1, 1, 1)
		_MainTexture("Main Texture", 2D) = "white" {}

		_LerpTime ("Lerp Time", Float) = 0
	}

	SubShader {
		Pass {
			CGPROGRAM

			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction

			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex:POSITION;
				float2 uv:TEXCOORD0;
			}

			struct v2f {
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
			}

			// vertex
			v2f vertexFunction(appdata IN) {
				v2f OUT;

				OUT.position = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.uv = IN.uv;

				return OUT;
			}

			float4 _Color1;
			float4 _Color2;
			sampler2D _MainTexture;
			float _LerpTime;

			// fragment
			fixed4 fragmentFunction(v2f IN) : SV_Target {

				float4 textureColor = tex2D(_MainTexture, IN.uv);

				float4 newColor = _Color1;

				return textureColor * newColor;
			}

			ENDCG
		}
	}
}