﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour {

    Animator anim;
    Animation animationComponent;

    int x = 0;

    void Start () {
        anim = GetComponent<Animator>();

	}
	
	void Update () {

        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

        if (Input.GetKeyUp(KeyCode.Space)) { 
            if (stateInfo.IsName("IdleAnim")) {
                Debug.Log("Now Playing: IdleAnim");
            } else if (stateInfo.IsName("ShowTent")) {
                Debug.Log("Now Playing: ShowTent");
            } else if (stateInfo.IsName("ShowRocks")) {
                Debug.Log("Now Playing: ShowRocks");
            }
        }

        if (stateInfo.IsName("IdleAnim") || stateInfo.IsName("ShowChest")) {
            if (Input.GetKeyUp(KeyCode.T)) {

                anim.SetTrigger("ShowTent");
                //anim.ResetTrigger("ShowTent");


            } else if (Input.GetKeyUp(KeyCode.R)) {

                anim.SetTrigger("ShowRocks");
                //anim.ResetTrigger("ShowRocks");


            } else if (Input.GetKeyUp(KeyCode.C)) {
                bool showChest = anim.GetBool("ShowChest");
                Debug.Log("ShowChest: " + showChest);
                anim.SetBool("ShowChest", !showChest);
            }
        }


	}
}
