﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMode : MonoBehaviour {

	public GameObject boat;
	public GameObject boatCamera;
	public GameObject player;
	public GameObject playerStartPositionBoat;

	public bool fpsMode = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp("e")) {
			fpsMode = !fpsMode;

			if (!fpsMode) {
				// BOAT MODE!
				boat.GetComponent<Rigidbody>().isKinematic = false;
				boat.GetComponent<BoatMovement>().enabled = true;
				boatCamera.SetActive(true);
				player.SetActive(false);
			} else {
				// ADVENTURING MODE!
				boat.GetComponent<Rigidbody>().isKinematic = true;
				boat.GetComponent<BoatMovement>().enabled = false;
				boatCamera.SetActive(false);
				player.SetActive(true);
				player.transform.position = playerStartPositionBoat.transform.position;
			}

		} 
		
	}
}
