﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLookAt : MonoBehaviour {

	public Camera mainCamera;

	private void Start() {
		mainCamera = Camera.main;
	}

	private void Update() {
		transform.LookAt(
			transform.position + mainCamera.transform.rotation * Vector3.back,
			mainCamera.transform.rotation * Vector3.up
		);
	}
}
