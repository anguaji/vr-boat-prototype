﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMovement : MonoBehaviour {

	private Rigidbody rBody;

	public float turnSpeed = 30f;
	public float accelerateSpeed = 200f;

	float horizontalAxis = 0f;
	float verticalAxis = 0f;

	void Start () {
		rBody = GetComponent<Rigidbody>();
	}
	
	void Update () {

		float h = horizontalAxis; //Input.GetAxis("Horizontal");
		float v = verticalAxis; //Input.GetAxis("Vertical");

        rBody.AddTorque(0f, h * turnSpeed * Time.deltaTime, 0f); // rotational force
		rBody.AddForce(transform.forward * v * accelerateSpeed * Time.deltaTime); // forward force

	}

	public void UpdateAxis(float h, float v) {
		horizontalAxis = h;
		verticalAxis = v;
		//Debug.Log("Update Boat Axis Input: " + h + ", " + v);
	}

}
