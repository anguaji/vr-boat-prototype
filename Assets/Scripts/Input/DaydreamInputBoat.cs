﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaydreamInputBoat : MonoBehaviour {

	public GameObject boat;

	BoatMovement boatMovement;

    float horizontalAxis = 0f;
	float verticalAxis = 0f;

    public bool anchored = false;

	void Start() {
		boatMovement = boat.GetComponent<BoatMovement>();

        horizontalAxis = 0;
        verticalAxis = 0;
	}

    public void ToggleAnchor() {

        Debug.Log("DaydreamInputBoat Instance: toggling anchor.");

        if (GameManager.instance.playerMode == GameMode.onBoat) {
            anchored = !anchored;

            if(anchored)
                boatMovement.UpdateAxis(0, 0); // set immediately
            
            Debug.Log("Anchored: " + anchored);
        } 
    }

    public void SetAnchor(bool anchored) {
        this.anchored = anchored;
        
    }


	void Update () {

        if (UIManager.instance.menuShowing || UIManager.instance.dialogShowing)
            boatMovement.UpdateAxis(0, 0);
        else 
            GetInput();

	}

    void GetInput() { 
        if (GameManager.instance.playerMode == GameMode.onBoat) {

            if (!anchored) {

                if (GvrController.State != GvrConnectionState.Connected) {

                    horizontalAxis = Input.GetAxis("Horizontal");
                    verticalAxis = Input.GetAxis("Vertical");

                } else { 
                    if (GvrController.ClickButton) {
                        verticalAxis = 1;
                    } else {
                        verticalAxis = 0;
                    }

                    Quaternion ori = GvrController.Orientation;
                    Vector3 oriVec = ori * Vector3.forward;
                    horizontalAxis = oriVec.x;
                }


            } else {
                verticalAxis = 0;
                horizontalAxis = 0;
            }

            boatMovement.UpdateAxis(horizontalAxis, verticalAxis);
        }
    }
}
