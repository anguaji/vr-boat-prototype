﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour {

	public float rotationSpeed = 100f;
	public float rotateAngle = 45f;

	public float swipeThreshold = 0.6f;
	public float swipeDuration = 0.5f;

	public bool smoothRotation = false;

	float swipeTimer = 0f;
	bool isSwiping;
	Vector2 defaultSwipePosition;

	float appButtonStartTime;
	float appButtonEndTime;

	public float appButtonLongPressThreshold = 1f;

	private float degree;
	private float angle;

	void Update () {

		Vector2 touchpos = GvrController.TouchPos;

		if (GvrController.IsTouching && isSwiping == false) {

			swipeTimer = 0;
			isSwiping = true;
			defaultSwipePosition = touchpos;

		} else if (GvrController.IsTouching && isSwiping == true) {

			swipeTimer += Time.deltaTime;

			if(swipeTimer > swipeDuration) {
				isSwiping = false;
				defaultSwipePosition = Vector2.zero;
				swipeTimer = 0f;
			}

		} else if ((!GvrController.IsTouching) && isSwiping == true) {
			
			// has just lifted finger

			Vector2 difference = defaultSwipePosition - touchpos;
			float swipeLength = difference.magnitude;

			Debug.Log("Swipe magnitude: " + swipeLength + " Difference: " + difference + " Time: " + swipeTimer);

			if (swipeLength > swipeThreshold) {
				float rotationAmount;
				if (defaultSwipePosition.x > touchpos.x) {
					// right
					rotationAmount = -rotateAngle;
				} else {
					// left
					rotationAmount = rotateAngle;
				}
				Debug.Log("Rotating " + rotationAmount + " degrees");

				RotateVRPlayer(rotationAmount);
			}
			
			isSwiping = false;
			defaultSwipePosition = Vector2.zero;
			swipeTimer = 0f;
			
		} 

		if(GvrController.AppButtonUp) {
			smoothRotation = !smoothRotation;
		}

		/*if (GvrController.AppButtonDown || Input.GetKeyDown(KeyCode.R)) {
			appButtonStartTime = Time.time;
		} else if(GvrController.AppButtonUp || Input.GetKeyUp(KeyCode.R)) {
			appButtonEndTime = Time.time;
			Debug.Log("Pressed for: " + (appButtonEndTime - appButtonStartTime));
			if((appButtonEndTime - appButtonStartTime) > appButtonLongPressThreshold) {
				RotateVRPlayer(rotateAngle);
			}
		}*/

		if(smoothRotation) {
			angle = Mathf.LerpAngle(transform.rotation.y, degree, Time.deltaTime * rotationSpeed);
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, degree, 0), Time.deltaTime * rotationSpeed);
		}

	}

	public void RotateVRPlayer(float rotationAmount) {

		if (!smoothRotation) {
			degree = 0;
			transform.Rotate(0, rotationAmount, 0);
		} else {
			degree += rotationAmount;
		}
	}
}
