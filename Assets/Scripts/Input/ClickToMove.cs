﻿using UnityEngine;
using UnityEngine.EventSystems;

    public class ClickToMove : MonoBehaviour {

        DaydreamInputLand playerNavMesh;
        public GameObject player;

        public Material inactiveMaterial;
        public Material gazedAtMaterial;
        Renderer rend;

        public void Start() {
            playerNavMesh = player.GetComponent<DaydreamInputLand>();
            rend = GetComponent<Renderer>();
        }

        public void SetGazedAt(bool gazedAt) {
        
            if (inactiveMaterial != null && gazedAtMaterial != null) {
                rend.material = gazedAt ? gazedAtMaterial : inactiveMaterial;
                return;
            }

            rend.material.color = gazedAt ? Color.green : Color.red;
        }

        public void TeleportTo(BaseEventData data) {
            //if (GameManager.instance.playerMode == GameMode.onFoot) {
                PointerEventData pointerData = data as PointerEventData;
                Vector3 worldPos = pointerData.pointerCurrentRaycast.worldPosition;
                Vector3 playerPos = new Vector3(worldPos.x, player.transform.position.y, worldPos.z);

                playerNavMesh.MovePlayer(playerPos);
            //} else {
            //    GameManager.instance.SwitchMode(GameMode.onFoot, null);
            //}
        }
    }
