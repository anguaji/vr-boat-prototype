﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DaydreamInputLand : MonoBehaviour {

	// point and click movement
	NavMeshAgent agent;

	// rotate player with swipe
	public float rotationSpeed = 15f;
	public float rotateAngle = 45f;

	public float swipeThreshold = 0.5f;
	public float swipeDuration = 1f;

	public bool smoothRotation = true;

	float swipeTimer = 0f;
	bool isSwiping;
	Vector2 defaultSwipePosition;

	float appButtonStartTime;
	float appButtonEndTime;

	private float degree;
	private float angle;

	void Start () {
		agent = GetComponent<NavMeshAgent>();
	}
	
	void Update () {

        if (UIManager.instance.menuShowing)
            return;

        GetInput();
	}

    void GetInput() { 
        
        // point and click
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, Camera.main.nearClipPlane)), out hit, 100)) {
                agent.destination = hit.point;
            }
        }

        // detect swipe
        Vector2 touchpos = GvrController.TouchPos;

        if (GvrController.IsTouching && isSwiping == false) {

            swipeTimer = 0;
            isSwiping = true;
            defaultSwipePosition = touchpos;

        } else if (GvrController.IsTouching && isSwiping == true) {

            swipeTimer += Time.deltaTime;

            if (swipeTimer > swipeDuration) {
                isSwiping = false;
                defaultSwipePosition = Vector2.zero;
                swipeTimer = 0f;
            }

        } else if ((!GvrController.IsTouching) && isSwiping == true) {

            // has just lifted finger

            Vector2 difference = defaultSwipePosition - touchpos;
            float swipeLength = difference.magnitude;

            //Debug.Log("Swipe magnitude: " + swipeLength + " Difference: " + difference + " Time: " + swipeTimer);

            if (swipeLength > swipeThreshold) {
                float rotationAmount;
                if (defaultSwipePosition.x > touchpos.x) {
                    // right
                    rotationAmount = -rotateAngle;
                } else {
                    // left
                    rotationAmount = rotateAngle;
                }
                RotateVRPlayer(rotationAmount);
            }

            isSwiping = false;
            defaultSwipePosition = Vector2.zero;
            swipeTimer = 0f;

        }

        //if (GvrController.AppButtonUp) {
        //    smoothRotation = !smoothRotation;
        //}

        if (smoothRotation) {
            angle = Mathf.LerpAngle(transform.rotation.y, degree, Time.deltaTime * rotationSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, degree, 0), Time.deltaTime * rotationSpeed);
        }
    }

	// point and click
	public void MovePlayer(Vector3 newPosition) {
		agent.destination = newPosition;
	}

	// rotate player
	public void RotateVRPlayer(float rotationAmount) {
		if (!smoothRotation) {
			degree = 0;
			transform.Rotate(0, rotationAmount, 0);
		} else {
			degree += rotationAmount;
		}
	}
}
