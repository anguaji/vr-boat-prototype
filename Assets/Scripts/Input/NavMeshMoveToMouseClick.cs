﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshMoveToMouseClick : MonoBehaviour {

    NavMeshAgent agent;

	void Start () {
        agent = GetComponent<NavMeshAgent>();	
	}
	
	void Update () {

        if (Input.GetMouseButtonDown(0)) {
            
            if (UIManager.instance.menuShowing || UIManager.instance.dialogShowing)
                return;
            
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, Camera.main.nearClipPlane)), out hit, 100)) {
                agent.destination = hit.point;
            }
        }
	}

    public void MovePlayer(Vector3 newPosition) {
        agent.destination = newPosition;
    }
}
