﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatClickHandler : MonoBehaviour {

	public void HandleClick() {
		if(GameManager.instance.playerMode == GameMode.onFoot) {
			Debug.Log("Entering boat mode!");
			GameManager.instance.SwitchMode(GameMode.onBoat);
		} else {
			Debug.Log("Clicked while in boat mode - disregard!");
		}
	}

}
