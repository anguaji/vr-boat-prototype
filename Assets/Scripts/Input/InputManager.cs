﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public static InputManager instance = null;

    public GameObject playerControllerModel;
    public bool controllerVisible = true;

    void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void SetControllerVisible(bool enabled) {
        controllerVisible = enabled;

        playerControllerModel.SetActive(controllerVisible);
    }

}
