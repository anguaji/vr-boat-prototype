﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaBlock : PoolObject {

    public GameObject seabed;
    public GameObject water;

    public bool isActive = false;

    public override void OnObjectReuse() {
        // use this to reset the object
        ShowSea(false);
    }

    void Start() {

        water = transform.Find("WaterPrefab").gameObject;
        seabed = transform.Find("SeaBed").gameObject;

        ShowSea(false);
    }

    void ShowSea(bool showSea) { 
        seabed.SetActive(true);
        water.SetActive(showSea);
        isActive = showSea;
    }

    void OnTriggerStay(Collider other) {
        if (other.tag == "PlayerSeaCheck") {
            if (isActive)
                return;

            ShowSea(true);
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "PlayerSeaCheck") {
            ShowSea(true);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "PlayerSeaCheck") {
            ShowSea(false);
        }
    }

}
