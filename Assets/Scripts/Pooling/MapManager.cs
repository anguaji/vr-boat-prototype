﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

    public NewSeaBlock newSeaBlockPrefab;
    public GameObject lowPolyWaterPrefab;

    public int sizeX = 10;
    public int sizeZ = 10;
    public int scale = 4;
    public int poolCapacity = 10;

    List<GameObject> map;// = new List<NewSeaBlock>();
	
    void Start () {

        // create the pool of the water
        PoolManager.instance.CreatePool(lowPolyWaterPrefab, poolCapacity);

        // instantiate this map position
        transform.position = Vector3.zero;

        map = new List<GameObject>();

        CreateSea();
	}

    public void CreateSea() {

        map = new List<GameObject>();
        
        int baseX = 0 - (sizeX / 2) - 1;
        int baseZ = 0 - (sizeZ / 2) - 1;

        for (int x = baseX; x < (sizeX / 2); x++) {
            for (int z = baseZ; z < (sizeZ / 2); z++) {
                
                Vector3 pos = new Vector3(x * scale, transform.position.y, z * scale);

                var newObject = Instantiate(newSeaBlockPrefab) as NewSeaBlock;
                newObject.transform.parent = transform;
                newObject.transform.position = pos;
                newObject.transform.rotation = Quaternion.identity;
                newObject.SetLowPolyWaterPrefab(lowPolyWaterPrefab);
                newObject.parentScale = scale * 2.5f;

                map.Add(newObject.gameObject);
            }
        }
    }

    public void DestroySea() { 
        
        foreach (var item in map) {
            if (item.gameObject != null)
                DestroyImmediate(item);

            //map.Remove(item);
        }
    }
}
