﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {

    // dictionary of many pools.
    Dictionary<int, Queue<ObjectInstance>> poolDictionary = new Dictionary<int, Queue<ObjectInstance>>();

    static PoolManager _instance;

    GameObject poolHolder;

    public static PoolManager instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<PoolManager>();
            }
            return _instance;
        }
    }

    public void CreatePool(GameObject prefab, int poolSize) {
        int poolKey = prefab.GetInstanceID();

        poolHolder = new GameObject(prefab.name + " pool");
        poolHolder.transform.position = Vector3.zero;
        poolHolder.transform.parent = transform;
        poolHolder.layer = 2;

        if (!poolDictionary.ContainsKey(poolKey)) {
            poolDictionary.Add(poolKey, new Queue<ObjectInstance>());

            for (int i = 0; i < poolSize; i++) {
                ObjectInstance newObject = new ObjectInstance(Instantiate(prefab) as GameObject);
                poolDictionary[poolKey].Enqueue(newObject);
                newObject.SetParent(poolHolder.transform);
                Debug.Log("Created pool object " + i);
            }
        }
    }

    public void ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation, GameObject objectParent = null) {
        int poolKey = prefab.GetInstanceID();

        if (poolDictionary.ContainsKey(poolKey)) {
            ObjectInstance objectToReuse = poolDictionary[poolKey].Dequeue(); // get first object out of queue

            if (objectToReuse.GetParent() != poolHolder.transform) {
                // move to poolHolder.transform
                objectToReuse.SetParent(poolHolder.transform);
            }

            poolDictionary[poolKey].Enqueue(objectToReuse);
            objectToReuse.Reuse(position, rotation);

            if (objectParent != null) {
                objectToReuse.SetParent(objectParent.transform);
            }

            Debug.Log("Id of used instance: " + poolKey);
        }
    }

    public void RepoolObject(GameObject instance) {
        Debug.Log("Repooling: " + instance.name + instance.GetInstanceID());
    }

    public class ObjectInstance {

        GameObject gameObject;
        Transform transform;

        bool hasPoolObjectComponent;
        PoolObject poolObjectScript;

        public ObjectInstance(GameObject objectInstance) {
            gameObject = objectInstance;
            transform = gameObject.transform;
            gameObject.SetActive(false);

            if (gameObject.GetComponent<PoolObject>()) {
                hasPoolObjectComponent = true;
                poolObjectScript = gameObject.GetComponent<PoolObject>();
            }
        }

        public void Reuse(Vector3 position, Quaternion rotation) {
            if (hasPoolObjectComponent) {
                poolObjectScript.OnObjectReuse();
            }

            gameObject.SetActive(true);
            transform.position = position;
            transform.rotation = rotation;
        }

        public void SetParent(Transform parent) {
            transform.parent = parent;
        }

        public Transform GetParent() {
            return gameObject.transform.parent;
        }


    }

}
