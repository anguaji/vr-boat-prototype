﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableWaterPrefab : PoolObject {

    public override void OnObjectReuse() {
        Debug.Log("Reusing: water prefab at " + transform.position);
    }

    public override void OnObjectRepool() {
        Debug.Log("Repooling: water prefab at " + transform.position);
    }
}
