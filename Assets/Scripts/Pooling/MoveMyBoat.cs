﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
    public float xMin, xMax, zMin, zMax;
}

[RequireComponent(typeof(Rigidbody))]
public class MoveMyBoat : MonoBehaviour {

    private Rigidbody rBody;

    public float turnSpeed = 30f;
    public float accelerateSpeed = 200f;

    float horizontalAxis = 0f;
    float verticalAxis = 0f;

    void Start() {
        rBody = GetComponent<Rigidbody>();
        Debug.Log("GVR Controller State: " + GvrController.State);
    }

    void Update() {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        rBody.AddTorque(0f, h * turnSpeed * Time.deltaTime, 0f); // rotational force
        rBody.AddForce(transform.forward * v * accelerateSpeed * Time.deltaTime); // forward force

    }
}
