﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapManager))]
public class MapManagerEditor : Editor {

    public override void OnInspectorGUI() {
        // base.OnInspectorGUI();

        DrawDefaultInspector();

        MapManager myScript = (MapManager)target;

        if (GUILayout.Button("Build Map")) {
            myScript.CreateSea();
        }

        if (GUILayout.Button("Destroy Map")) {
            myScript.DestroySea();
        }
    }
	
}
