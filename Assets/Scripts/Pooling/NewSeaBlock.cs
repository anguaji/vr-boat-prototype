﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class NewSeaBlock : MonoBehaviour {

    GameObject lowPolyWaterPrefab;
    GameObject lowPolyWaterInstance;
    bool isActive = false;
    BoxCollider boxCollider;
    public float parentScale = 1;

    public void Start() {
        boxCollider = GetComponent<BoxCollider>();
        boxCollider.size = new Vector3(parentScale, parentScale, parentScale);
    }

    public void SetLowPolyWaterPrefab(GameObject prefab) {
        lowPolyWaterPrefab = prefab;
    }

	void OnTriggerStay(Collider other) {
        if (other.tag == "PlayerSeaCheck") {
            if (isActive)
                return;
            Debug.Log("OnTriggerStay PlayerSeaCheck");

            isActive = true;
            ShowLowPolyWater();
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "PlayerSeaCheck" && isActive != true) {
            Debug.Log("OnTriggerEnter PlayerSeaCheck");
            isActive = true;
            ShowLowPolyWater();
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "PlayerSeaCheck") {
            Debug.Log("OnTriggerExit PlayerSeaCheck");
            isActive = false;
            HideLowPolyWater();
        } else if (other.tag == "PlayerSeaCull") {
            Debug.Log("OnTriggerExit PlayerSeaCull");
            isActive = false;
            HideLowPolyWater();
        }
    }

    void ShowLowPolyWater() {
        if(gameObject.transform.childCount < 1) 
            PoolManager.instance.ReuseObject(lowPolyWaterPrefab, transform.position, transform.rotation, gameObject);
    }

    void HideLowPolyWater() { 

        if (gameObject.transform.childCount > 0) {
            for (int x = 0; x <= gameObject.transform.childCount - 1; x++) {
                var item = gameObject.transform.GetChild(x);
                if (item.tag == "WaterPrefab") {
                    PoolManager.instance.RepoolObject(item.gameObject);
                }
            }
        }
    }
}
