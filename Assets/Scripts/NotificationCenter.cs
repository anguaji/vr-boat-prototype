﻿using System.Collections.Generic;
using System;

public class NotificationCenter {
    public static Dictionary<string, Action> Actions = new Dictionary<string, Action>();

    public static void Sub(string name, Action a) {
        Actions.Add(name, a);
    }

    public static void Fire(string name) {
        Actions[name]();
    }
}