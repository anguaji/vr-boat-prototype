﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState {
	inGame,
	won,
	paused
}

public enum GameMode {
	onFoot,
	onBoat
}

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

	public GameObject player;
	public GameObject boat;
    public GameObject boatModel;

	public GameMode playerMode = GameMode.onFoot;
	public GameObject gvrPointer;

	public Transform boatPosition;
	public Transform landPosition;

	DaydreamInputBoat inputBoat;
	DaydreamInputLand inputLand;
    Renderer boatRenderer;

    Canvas fpsCounter;
    public bool showFPS = false;
    bool fpsEnabled = false;
    public GameObject fpsHolder;

    public AudioSource bgmWaves;

	public List<Item> gameItems;

    int timesInBoat = 0;
    string[] thankyouMessage = new string[5];
	
    void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }

	private void Start() {

        //this.SendMessage

        fpsHolder = GameObject.Find("FPSCount");
        if (fpsHolder != null) {
            fpsCounter = fpsHolder.GetComponent<Canvas>();
            fpsEnabled = true;
            fpsHolder.SetActive(showFPS);
        }

		inputBoat = player.GetComponent<DaydreamInputBoat>();
		inputLand = player.GetComponent<DaydreamInputLand>();

        boatRenderer = boatModel.GetComponent<Renderer>();

		gameItems = new List<Item>();

		gameItems.Add(new Item("Compass", false)); // 0
		gameItems.Add(new Item("Spyglass", false)); // 1
		gameItems.Add(new Item("Bottle Of Grog", false)); // 2
		gameItems.Add(new Item("Cannon Balls", false)); // 3
		gameItems.Add(new Item("Pirate Hook", false)); // 4

        thankyouMessage[0] = "Many thanks <i>- Mrs Teach</i>";
        thankyouMessage[1] = "What a good pirate! <i>- Mrs Teach</i>";
        thankyouMessage[2] = "It's about time. <i>- Mrs Teach</i>";
        thankyouMessage[3] = "Nearly there! <i>- Mrs Teach</i>";
        thankyouMessage[4] = "Just one more! <i>- Mrs Teach</i>";

		InitGame();
	}
	
    void InitGame() {
		SwitchMode(playerMode);
        UIManager.instance.ShowDialogBox("Blackbeard lost his treasure!\nFind it!\n- yrs, Mrs Teach");
    }

    void Update() {

        if (GvrController.AppButtonUp || Input.GetKeyUp(KeyCode.T)) {
            if (playerMode == GameMode.onBoat) {
                ToggleAnchor();
            }
        }

        if (Input.GetKeyUp(KeyCode.R)) {
            if (playerMode == GameMode.onFoot) {
                SwitchMode(GameMode.onBoat);
            } else { 
                SwitchMode(GameMode.onFoot);
            }
        }
        
    }

    public void ToggleAnchor() {
        Debug.Log("GameManager Instance: toggling anchor.");
        inputBoat.ToggleAnchor();

        if (inputBoat.anchored) {
            UIManager.instance.ShowDialogBox("Anchor dropped.\nYou'll remain in place until\nyou raise the anchor again.");
        } else { 
            UIManager.instance.ShowDialogBox("Anchors aweigh!\nSteer the boat by pointing.\nClick the touchpad to move.");
        }
    }

    public bool IsAnchored { 
        get {
            return inputBoat.anchored;
        }
    }

	public void CollectItem(int itemID) {
        if (itemID < gameItems.Count) {
            gameItems[itemID].collected = true;
            Debug.Log("Collected " + gameItems[itemID].title);
        } else {
            return;
        }

        if (HasCollectedAllItems()) {
            UIManager.instance.ShowDialogBox("<b>Shiver Me Timbers!</b>\n\nYou collected the " + gameItems[itemID].title + "!\nand you found all the treasure!\n\n<i>Many thanks from the Blackbeards</i>");
        } else {
            UIManager.instance.ShowDialogBox("You found the " + gameItems[itemID].title + "!\n\n" + thankyouMessage[CountCollectedItems()]);
        }
	}

    int CountCollectedItems() { 
        int collectedItems = 0;

        foreach (var item in gameItems) {
            if (item.collected)
                collectedItems++;
        }

        return collectedItems;
    }

	bool HasCollectedAllItems() {
        int collectedItems = CountCollectedItems();
		
        if (collectedItems >= gameItems.Count)
            return true;
        else
            return false;
	}

    public void SwitchMode(GameMode gameMode, Transform landingPosition = null) {
		playerMode = gameMode;

		if (playerMode == GameMode.onFoot) {

            bgmWaves.volume = 0.1f;
            
			player.transform.SetParent(null, true);
			inputBoat.enabled = false;
			inputLand.enabled = true;
			gvrPointer.SetActive(true);
			
            boat.GetComponent<Rigidbody>().isKinematic = true;

            player.GetComponent<CapsuleCollider>().enabled = true;
			player.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
			
            player.GetComponent<Rigidbody>().isKinematic = false;

            if (landingPosition != null) {
                Debug.Log("Landed on another island");
                player.transform.position = landingPosition.position;
                player.transform.rotation = landingPosition.rotation;
            } else { 
                Debug.Log("Landed on home island");
                player.transform.position = landPosition.position;
                player.transform.rotation = landPosition.rotation;
            }

            boatRenderer.material.renderQueue = 1000;
            boat.GetComponent<Inspect>().guiDescription.enabled = true;

            inputBoat.SetAnchor(true);

        } else {

            timesInBoat++;

            bgmWaves.volume = 0.2f;

            player.transform.SetParent(boatPosition, true);
			player.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
			player.GetComponent<CapsuleCollider>().enabled = false;

            inputBoat.enabled = true;
			inputLand.enabled = false;
			gvrPointer.SetActive(true);
			
            boat.GetComponent<Rigidbody>().isKinematic = false;
			
            player.GetComponent<Rigidbody>().isKinematic = true;

			player.transform.position = boatPosition.position;
			player.transform.rotation = boatPosition.rotation;

            boatRenderer.material.renderQueue = 4000;
            boat.GetComponent<Inspect>().guiDescription.enabled = false;

            inputBoat.SetAnchor(false);

            if (timesInBoat < 2) {
                UIManager.instance.ShowDialogBox("Ahoy there!\nClick the touchpad to move your boat.\nPoint with the controller to steer.\nUse the app button to drop/raise anchor.");
            }

		}
	}
}
