﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour {

    Animator anim;
    bool isOpen = false;

    void Start () {
        anim = GetComponent<Animator>();
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Debug.Log("Pressed space");
            isOpen = !isOpen;

            if (isOpen)
                OpenBox();
            else
                CloseBox();
        }
    }

    public void OpenBox() {
        anim.SetTrigger("OpenBox");
    }

    public void CloseBox() {
        anim.SetTrigger("CloseBox");
    }
}
