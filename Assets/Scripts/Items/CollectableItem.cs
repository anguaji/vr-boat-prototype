﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableItem : MonoBehaviour {

	// renderer events
	public Material outlineMaterial;
	Material originalMaterial;
	Renderer rend;

	public int id;
	public string title;

	void Start() {
		rend = GetComponent<Renderer>();
		originalMaterial = rend.material;
	}

	private void OnMouseOver() {
		SetGazedAt(true);
	}

	private void OnMouseExit() {
		SetGazedAt(false);
	}

	public void SetGazedAt(bool gazed) {
		if (gazed) {
			rend.material = outlineMaterial;
		} else {
			rend.material = originalMaterial;
		}
	}

	public void CollectItem() {
		// send message to game manager
		GameManager.instance.CollectItem(this.id);
		Destroy(gameObject);
	}

}
