﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inspect : MonoBehaviour {

    public string tooltipText = "Object";
	public Canvas guiCanvas;
	public Text guiDescription;
    public bool showDescriptionFromBoat = true;

    void Start() {
		guiDescription.text = tooltipText;
		guiCanvas.enabled = false;
	}

    public void SetGazedAt(bool gazedAt) {
        if (UIManager.instance.menuShowing || UIManager.instance.dialogShowing) {
            guiCanvas.enabled = false;
            return;
        }

        if(GameManager.instance.playerMode == GameMode.onFoot || (GameManager.instance.playerMode == GameMode.onBoat && showDescriptionFromBoat))
			guiCanvas.enabled = gazedAt;
    }

    public void InspectItem() { 
        Debug.Log("Clicked on " + tooltipText + "!");
    }
}
