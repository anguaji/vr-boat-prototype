﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hold : MonoBehaviour {

    // position
    public Vector3 idlePosition;
    public Vector3 examinedPosition;
    public Vector3 heldPosition;
    public Quaternion idleRotation;
    public float examinedRotation = -12.8f;

    // player
    public GameObject playerCamera;

    // renderer
    public Material outlineMaterial;
    private Material originalMaterial;
    private Renderer rend;

    // examined
    public bool beingExamined = false;
    public bool beingHeld = false;
	
    void Start () {
        idlePosition = transform.position;
        idleRotation = transform.rotation;
        rend = GetComponent<Renderer>();
        originalMaterial = rend.material;
	}	
	
    void Update () {

        if (beingHeld) {

            if (beingExamined) {
                Quaternion ori = GvrController.Orientation;
                transform.localRotation = ori;

                if (GvrController.AppButtonUp)
                    SetExamined(false);
                
            } else { 
                if (GvrController.AppButtonDown)
                    SetExamined(true);
            }

        }

	}

    public void SetHold(bool held) {

        if (held == true && beingHeld == true)
            held = false;

        Debug.Log("SetHold: " + held);

        SetGazedAt(false);
        beingHeld = held;

        if (beingHeld) {
            transform.parent = playerCamera.transform;
            transform.localPosition = heldPosition;
            transform.rotation = Quaternion.identity;
        } else {
            transform.parent = null;
            transform.position = idlePosition;
            transform.rotation = idleRotation;
        }
    }

    public void SetExamined(bool examined) {

        SetGazedAt(false);
        beingExamined = examined;

        if (beingExamined) {
            InputManager.instance.SetControllerVisible(false);
            transform.localPosition = examinedPosition;
            transform.rotation = Quaternion.identity;
        } else {
            InputManager.instance.SetControllerVisible(true);
            transform.localPosition = heldPosition;
            transform.rotation = Quaternion.identity;
        }
    }

    public void SetGazedAt(bool gazed) {

        if (beingExamined || beingHeld)
            return;
        
        if (gazed) {
            rend.material = outlineMaterial;
        } else {
            rend.material = originalMaterial;
        }
    }
}
