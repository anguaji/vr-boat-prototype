﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item {

	public string title;
	public bool collected;

	public Item(string _title, bool _collected) {
		title = _title;
		collected = _collected;
	}

}
