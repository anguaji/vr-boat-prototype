﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campfire : MonoBehaviour {

    public bool isLit = false;

    public GameObject fireSystem;


    public ParticleSystem fireParticleSystem;
    public Light fireLight;

    ParticleSystem.EmissionModule fireEmissions;
    ParticleSystem.MinMaxCurve defaultParticleRateOverTime;

    void Start () {
        fireEmissions = fireParticleSystem.emission;
        defaultParticleRateOverTime = fireEmissions.rateOverTime;

        SetFire(isLit);
	}
	
	void Update () {
		
	}

    void SetFire(bool lit) {
        isLit = lit;

        fireSystem.SetActive(isLit);

    }

    public void ToggleFire() {
        isLit = !isLit;
        SetFire(isLit);
    }


}
