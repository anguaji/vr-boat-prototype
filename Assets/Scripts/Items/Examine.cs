﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Examine : MonoBehaviour {

    // position
    public Vector3 idlePosition;
    public Vector3 examinedPosition;
    public Quaternion idleRotation;

    // player
    public GameObject player;

    // renderer
    public Material outlineMaterial;
    private Material originalMaterial;
    private Renderer rend;

    // examined
    public bool beingExamined = false;
	
    void Start () {
        idlePosition = transform.position;
        idleRotation = transform.rotation;
        rend = GetComponent<Renderer>();
        originalMaterial = rend.material;
	}	
	
    void Update () {

        if (beingExamined) {
            
            Quaternion ori = GvrController.Orientation;

            transform.localRotation = ori;

            if (GvrController.AppButtonUp)
                SetExamined(false);
        }

	}

    public void SetExamined(bool examined) {

        SetGazedAt(false);

        beingExamined = examined;

        if (beingExamined) {
            InputManager.instance.SetControllerVisible(false);
            transform.parent = player.transform;
            transform.localPosition = examinedPosition;
            transform.rotation = Quaternion.identity;
        } else {
            InputManager.instance.SetControllerVisible(true);
            transform.parent = null;
            transform.position = idlePosition;
            transform.rotation = idleRotation;
        }
    }

    public void SetGazedAt(bool gazed) {

        if (beingExamined)
            return;
        
        if (gazed) {
            rend.material = outlineMaterial;
        } else {
            rend.material = originalMaterial;
        }
    }
}
