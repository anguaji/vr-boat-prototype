﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject bootyMenu;
    public GameObject campMenu;

    public Button anchorButton;
    public Text anchorButtonText;

    public Text descriptionText;

    void Start () {
        HideAllMenus();
	}

    void Update() { 
        if (Input.GetKeyDown(KeyCode.Escape))
            ToggleMenu();
    }

    void ToggleMenu() {
        mainMenu.SetActive(!mainMenu.activeInHierarchy);
    }

    public void DropAnchor() {

        Debug.Log("Clicked anchor button");

        if (GameManager.instance.playerMode == GameMode.onBoat) {
            
            GameManager.instance.ToggleAnchor();

            if (GameManager.instance.IsAnchored) {
                descriptionText.text = "Anchor dropped: your boat is now fixed.";
                anchorButtonText.text = "Raise Anchor!";
            } else { 
                descriptionText.text = "Anchor raised: you can now sail to adventure!";
                anchorButtonText.text = "Drop Anchor!";
            }

        } 

        //HideAllMenus();
    }

    public void ShowBootyList() {
        HideAllMenus();
        bootyMenu.SetActive(true);
    }

    public void ShowCampfireList() {
        HideAllMenus();
        campMenu.SetActive(true);
    }

    public void ShowMainMenu() {

        if (GameManager.instance.playerMode == GameMode.onBoat) {
            anchorButton.enabled = true;
        } else {
            anchorButton.enabled = false;
        }

        HideAllMenus();
        mainMenu.SetActive(true);

    }

    public void HideAllMenus() { 
        mainMenu.SetActive(false);
        bootyMenu.SetActive(false);
        campMenu.SetActive(false);
    }


}
