﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public static UIManager instance = null;

    public MainMenu mainMenuInstance;
    public DialogBox dialogBoxInstance;

    public GameObject player;
    public bool menuShowing = false;
    public bool dialogShowing = false;

    void Awake() { 
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

	void Start () {

        mainMenuInstance.gameObject.SetActive(false);
        menuShowing = false;
	}

    public void ToggleMenu() {

        menuShowing = !mainMenuInstance.gameObject.activeInHierarchy;

        ShowMenu(menuShowing);
    }

    public void ShowMenu(bool show) {

        menuShowing = show;

        if (menuShowing)
            mainMenuInstance.ShowMainMenu();

        mainMenuInstance.gameObject.SetActive(menuShowing);
    }

    public void ShowDialogBox(string message) {

        dialogBoxInstance.SetDialogMessage(message);
        dialogBoxInstance.Open();
    }

}
