﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCounter : MonoBehaviour {

    public UnityEngine.UI.Text guiTextDisplay;

	float frameCount = 0f;
    float dt = 0.0f;
    float fps = 0.0f;
    float updateRate = 4.0f;  // 4 updates per sec.

    void Update() {
        
        frameCount++;

        dt += Time.deltaTime;

        if (dt > 1.0f / updateRate) {
            fps = frameCount / dt;
            frameCount = 0f;
            dt -= 1.0f / updateRate;
            guiTextDisplay.text = "FPS: " + fps;
        }
    }
}

