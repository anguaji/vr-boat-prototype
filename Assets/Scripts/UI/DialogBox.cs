﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour {

    public GameObject dialogHolder;
    public Text dialogText;
    public string dialogMessage;
    public AudioSource sfx;

    void Start () {
        SetDialogMessage(dialogMessage);
        //dialogHolder.SetActive(false);
	}

    public void SetDialogMessage(string newMessage) {
        dialogMessage = newMessage;
        dialogText.text = dialogMessage;
    }

    public void Open() {
        UIManager.instance.dialogShowing = true;
        dialogHolder.SetActive(true);
        sfx.pitch = 1f;
        sfx.Play();
    }

    public void Close() {
        UIManager.instance.dialogShowing = false;
        sfx.pitch = 0.8f;
        sfx.Play();
        dialogHolder.SetActive(false);
    }

}
