﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject : MonoBehaviour {

    public Material defaultMaterial;
    public Material highlightedMaterial;

    Renderer rend;

    void Start () {
        rend = GetComponent<Renderer>();	
	}

    public void SetGazedAt(bool gazed) {
        
        if (gazed)
            rend.material = highlightedMaterial;
        else
            rend.material = defaultMaterial;
        
    }
}
