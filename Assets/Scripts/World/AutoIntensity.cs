﻿using UnityEngine;
using System.Collections;

public class AutoIntensity : MonoBehaviour {

	public Gradient nightDayColor;

	public GameObject stars;
	ParticleSystem starParticles;
	ParticleSystem.EmissionModule starEmissions;

	public float realTimeOfDay;
    public bool sunSetting = true;

	float i = 0;

	public float maxIntensity = 3f;
	public float minIntensity = 0f;
	public float minPoint = -0.2f;

	public float maxAmbient = 1f;
	public float minAmbient = 0f;
	public float minAmbientPoint = -0.2f;

    public float days = 0;

	public Gradient nightDayFogColor;
	public AnimationCurve fogDensityCurve;
	public float fogScale = 1f;

	public float dayAtmosphereThickness = 0.4f;
	public float nightAtmosphereThickness = 0.87f;

	public Vector3 dayRotateSpeed;
	public Vector3 nightRotateSpeed;

    public AudioSource bgmNightMusic;
    int daysPassed = 0;

	float skySpeed = 1;


	Light mainLight;
	Skybox sky;
	Material skyMat;

	void Start() {
		mainLight = GetComponent<Light>();
		skyMat = RenderSettings.skybox;

		starParticles = stars.GetComponent<ParticleSystem>();
		starEmissions = starParticles.emission;
		starEmissions.rateOverTime = 0f;
	}

	void Update() {
		
		float tRange = 1 - minPoint;
        float oldTimeOfDay = realTimeOfDay; // save the real time of day from last frame
        bool oldSetting = sunSetting;

        realTimeOfDay = (Vector3.Dot(mainLight.transform.forward, Vector3.down) - minPoint) / tRange;

        if (oldTimeOfDay < realTimeOfDay) {
            sunSetting = false;
        } else {
            sunSetting = true;
        }

        if (oldSetting != sunSetting) {
            days += 0.5f;
        }

        // mainLight
        float mainLightValue = Mathf.Clamp01((Vector3.Dot(mainLight.transform.forward, Vector3.down) - minPoint) / tRange);
		mainLight.intensity = ((maxIntensity - minIntensity) * mainLightValue) + minIntensity;
        tRange = 1 - minAmbientPoint;
		
        // ambientIntensity
        float dot = Mathf.Clamp01((Vector3.Dot(mainLight.transform.forward, Vector3.down) - minAmbientPoint) / tRange);
		mainLight.color = nightDayColor.Evaluate(dot);
		
        RenderSettings.ambientIntensity = ((maxAmbient - minAmbient) * dot) + minAmbient;
        RenderSettings.ambientLight = mainLight.color;
        RenderSettings.fogColor = nightDayFogColor.Evaluate(dot);
		RenderSettings.fogDensity = fogDensityCurve.Evaluate(dot) * fogScale;

        skyMat.SetFloat("_AtmosphereThickness", ((dayAtmosphereThickness - nightAtmosphereThickness) * dot) + nightAtmosphereThickness);

        if (realTimeOfDay <= 0) {
            transform.Rotate(nightRotateSpeed * Time.deltaTime * skySpeed);
        } else if (realTimeOfDay > 0) {
			transform.Rotate(dayRotateSpeed * Time.deltaTime * skySpeed);
		}

        float floatStarEmissions = System.Math.Abs(starEmissions.rateOverTime.constantMax);

        if (sunSetting && realTimeOfDay < 0.4f && realTimeOfDay > 0.2f) {
            starEmissions.rateOverTime = 10;
        } else if (sunSetting && realTimeOfDay < 0.2f) {
            starEmissions.rateOverTime = 30f;
        } else if (!sunSetting && realTimeOfDay > -0.25f) { 
            starEmissions.rateOverTime = 0f;
        }

        if (sunSetting && realTimeOfDay < 0.3f && days < 1.5f && bgmNightMusic.isPlaying == false) {
            bgmNightMusic.Play();
        }

        //Debug.Log("Sun is setting: " + sunSetting + " Realtime: " + realTimeOfDay + ", stars: " + floatStarEmissions);

        if (Input.GetKeyDown(KeyCode.Q)) skySpeed *= 0.5f;
		if (Input.GetKeyDown(KeyCode.E)) skySpeed *= 2f;

	}
}