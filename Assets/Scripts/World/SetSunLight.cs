﻿using UnityEngine;
using System.Collections;

public class SetSunLight : MonoBehaviour {

	Material sky;

	public Transform stars;
	public Transform worldProbe;
    public bool rotateStars = true;

	void Start() {
		sky = RenderSettings.skybox;
	}

	void Update() {

        if(rotateStars)
		    stars.transform.rotation = transform.rotation;

		Vector3 tvec = Camera.main.transform.position;
		worldProbe.transform.position = tvec;

	}
}