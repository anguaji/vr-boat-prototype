﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightController : MonoBehaviour {

    public Light sun;
    public float secondsInFullDay = 120f;
    [Range(0, 1)]
    public float currentTimeOfDay = 0f;
    [Range(0, 24)]
    public float realTimeOfDay = 0;
    [HideInInspector]
    public float timeMultiplier = 1f;
    public float sunInitialIntensity;

    public Material[] skyboxes = new Material[5];

    void Start() {
        sunInitialIntensity = sun.intensity;
    }

    void Update() {
        UpdateSun();

        currentTimeOfDay += (Time.deltaTime / secondsInFullDay) * timeMultiplier;

        if (currentTimeOfDay >= 1)
            currentTimeOfDay = 0;
    }

    void UpdateSun() {
        sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);

        float intensityMultiplier = 1;

        if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f)
            intensityMultiplier = 0;
        else if (currentTimeOfDay <= 0.25f)
            intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
        else if (currentTimeOfDay >= 0.73f)
            intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));

        sun.intensity = sunInitialIntensity * intensityMultiplier;
        realTimeOfDay = ConvertTime(currentTimeOfDay);

        //UpdateScene();

    }

    void UpdateScene() { 
        if (currentTimeOfDay < 0.25) {
            RenderSettings.skybox = skyboxes[0]; // midnight
        } else if (currentTimeOfDay >= 0.25 && currentTimeOfDay < 0.4) {
            RenderSettings.skybox = skyboxes[1]; // daybreak
        } else if (currentTimeOfDay >= 0.4 && currentTimeOfDay < 0.7) {
            RenderSettings.skybox = skyboxes[2]; // midday
        } else if (currentTimeOfDay >= 0.7 && currentTimeOfDay < 0.9) {
            RenderSettings.skybox = skyboxes[3]; // sunset
        } else if (currentTimeOfDay >= 0.9) {
            RenderSettings.skybox = skyboxes[4]; // evening
        }
    }

    float ConvertTime(float percentage) {
        float timeOfDay;

        float totalHours = 24f;

        timeOfDay = percentage * totalHours;

        return timeOfDay;
    }


}
